using System;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SummaryService.Data;
using SummaryService.Core.Entities;

namespace SummaryService.WebApi
{
    [ApiController]
    [Route("api/[controller]")]
    public class SummaryController: ControllerBase
    {
        private readonly SummaryContext _summaryContext;

        public SummaryController(SummaryContext summaryContext)
        {
            _summaryContext = summaryContext;
        }

        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadGateway)]
        public Summary Get()
        {
            Summary summary = new Summary();

            summary.StudentsByRating = this._summaryContext.StudentsRating
                .GroupBy(p => p.Name)
                .Select(g => new { rating = g.Key, max = g.Max() })
                .AsNoTracking()
                .ToDictionary(k => k.rating, i => Convert.ToDouble(i.max));

            summary.TeamsByRating = this._summaryContext.TeamsRating
                .GroupBy(p => p.Name)
                .Select(g => new { rating = g.Key, max = g.Max() })
                .AsNoTracking()
                .ToDictionary(k => k.rating, i => Convert.ToDouble(i.max));

            return summary;
        }
    }
}