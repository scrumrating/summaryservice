using Microsoft.EntityFrameworkCore;
using SummaryService.Core;
using SummaryService.Core.DTO;

namespace SummaryService.Data
{
    public class SummaryContext: DbContext
    {
        private readonly string connectionString;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="SummaryContext"/>.
        /// </summary>
        /// <param name="connectionString">Строка подключения.</param>
        public SummaryContext(DbContextOptions<SummaryContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        /// <summary>
        /// Получает содержимое базы данных StudentsRating.
        /// </summary>
        public DbSet<StudentsRatingDto> StudentsRating { get; }

        /// <summary>
        /// Получает содержимое базы данных TeamsRating.
        /// </summary>
        public DbSet<TeamsRatingDto> TeamsRating { get; }

        /// <inheritdoc/>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(this.connectionString);
        }
    }
}