using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SummaryService.Core.DTO;
using SummaryService.Core.Interfaces;

namespace SummaryService.Data.Repositories
{
    /// <summary>
    /// Реализация репозитория.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class EFGenericRepository<TEntity> : IRepository<TEntity> where TEntity : IdentityDto
    {
        readonly DbContext context;
        readonly DbSet<TEntity> dbSet;

        /// <summary>
        /// Конструктор класса.
        /// </summary>
        /// <param name="context">Контекст базы данных.</param>
        public EFGenericRepository(DbContext context)
        {
            this.context = context;
            this.dbSet = context.Set<TEntity>();
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<TEntity>> GetAsync()
        {
            return await this.dbSet.ToListAsync();
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<TEntity>> GetAsync(int pageSize, int pageNumber)
        {
            return await this.dbSet.Where(s => s.IsDeleted == false)
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<TEntity>> GetAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await this.dbSet.Where(predicate).ToListAsync();
        }

        /// <inheritdoc/>
        public async Task<TEntity> FindByIdAsync(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentException("Не может быть меньше 0.");
            }

            return await this.dbSet.FindAsync(id);
        }

        /// <inheritdoc/>
        public async Task<bool> CreateAsync(TEntity item)
        {
            if (item == null)
            {
                throw new NullReferenceException("Не может быть null.");
            }

            await this.dbSet.AddAsync(item);
            await SaveChangesAsync();
            return true;
        }

        /// <inheritdoc/>
        public async Task UpdateAsync(TEntity item)
        {
            if (item == null)
            {
                throw new ArgumentException("Не может быть null.");
            }

            this.context.Entry(item).State = EntityState.Modified;
            await SaveChangesAsync();
        }

        /// <inheritdoc/>
        public async Task RemoveAsync(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentException("Не может быть меньше 0.");
            }

            var item = await FindByIdAsync(id);
            this.dbSet.Remove(item);
            await SaveChangesAsync();
        }

        /// <summary>
        /// Сохранение записей.
        /// </summary>
        /// <returns>Задача.</returns>
        public async Task SaveChangesAsync()
        {
            await this.context.SaveChangesAsync();
        }
    }
}