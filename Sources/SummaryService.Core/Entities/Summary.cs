using System.Collections.Generic;

namespace SummaryService.Core.Entities
{
    /// <inheritdoc cref="" />
    public class Summary
    {
        public Dictionary<string, double> StudentsByRating { get; set; }

        public Dictionary<string, double> TeamsByRating { get; set; }

        public Summary()
        {
            StudentsByRating = new Dictionary<string, double>();
            TeamsByRating = new Dictionary<string, double>();
        }
    }
}