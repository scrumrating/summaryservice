using System;
using System.Collections.Generic;

namespace SummaryService.Core.Interfaces
{
     /// <summary>
    /// Сервисный объект для управления Team.
    /// </summary>
    public interface IGenericService<TEntity> where TEntity : class
    {
        /// <summary>
        /// Создание записи.
        /// </summary>
        /// <param name="item">Сущность для создания.</param>
        /// <returns>Успешное добавление.</returns>
        bool Create(TEntity item);

        /// <summary>
        /// Найти и вернуть сущность по ID.
        /// </summary>
        /// <param name="id">Чиcловой идентификатор записи.</param>
        /// <returns>Найденная сущность или null.</returns>
        TEntity FindById(int id);

        /// <summary>
        /// Получить коллекцию строк.
        /// </summary>
        /// <returns>Выборка данных.</returns>
        IEnumerable<TEntity> Get();

        /// <summary>
        /// Получить коллекцию строк. (Пагинация)
        /// </summary>
        /// <param name="pageSize">Количество строк для получения.</param>
        /// <param name="pageNumber">Номер страницы.</param>
        /// <returns>Выборка данных.</returns>
        IEnumerable<TEntity> Get(int pageSize, int pageNumber);

        /// <summary>
        /// Получить коллекцию строк по заданному фильтру.
        /// </summary>
        /// <param name="predicate">Условие по кторому будут браться даныне.</param>
        /// <returns>Выборка данных.</returns>
        IEnumerable<TEntity> Get(Func<TEntity, bool> predicate);

        /// <summary>
        /// Удалить запись.
        /// </summary>
        /// <param name="item">Сущность для удаления.</param>
        void Remove(TEntity item);

        /// <summary>
        /// Измменить сущность.
        /// </summary>
        /// <param name="item">Обновление записи.</param>
        void Update(TEntity item);
    }
}