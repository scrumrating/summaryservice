namespace SummaryService.Core.DTO
{
    public class IdentityDto
    {
        /// <summary>
        /// Получает или задает идентификатор команды.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Получает или задает значение, показывающее, что группа удалена.
        /// </summary>
        public bool IsDeleted { get; set; }
    }
}