namespace SummaryService.Core.DTO
{
    public class StudentsRatingDto: IdentityDto
    {
        /// <summary>
        /// Получает или задает имя студента.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Получает или задает рейтинг студента.
        /// </summary>
        public decimal Rating { get; set; }
    }
}