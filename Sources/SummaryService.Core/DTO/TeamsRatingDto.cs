using SummaryService.Core.DTO;

namespace SummaryService.Core
{
    public class TeamsRatingDto: IdentityDto
    {
        /// <summary>
        /// Получает или задает имя команды.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Получает или задает рейтинг команды.
        /// </summary>
        public decimal Rating { get; set; }
    }
}