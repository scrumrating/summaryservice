﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using SummaryService.Data;
using SummaryService.WebApi;

namespace SummaryService.Instance
{
    /// <summary>
    /// Инициализация приложения.
    /// </summary>
    public class Startup
    {
        private readonly IConfiguration configuration;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="Startup"/>.
        /// </summary>
        public Startup(IHostingEnvironment environment)
        {
            this.configuration = configuration;
            ConfigureDataStorage();
        }

        /// <summary>
        /// Конфигурирует базу данных.
        /// </summary>
        private void ConfigureDataStorage()
        {
            Log.Information("Начинается создание/обновление базы данных.");

            var contextOptions = new DbContextOptionsBuilder<SummaryContext>();
            contextOptions.UseNpgsql(this.configuration.GetConnectionString("Teams"));

            using (var context = new SummaryContext(contextOptions.Options))
            {
                context.Database.EnsureCreated();
            }

            Log.Information("Создание/обновление базы данных завершено.");
        }

        /// <summary>
        /// Конфигурация приложения.
        /// </summary>
        /// <param name="application">Приложение.</param>
        public void Configure(IApplicationBuilder application)
        {
            application.UseCors("AllowAll");

            application.UseMvc();
            application.UseSwaggerDocumentation(this.configuration);
        }

        /// <summary>
        /// Конфигурация сервисов.
        /// </summary>
        /// <param name="services">Сервисы.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(this.configuration)
                .CreateLogger();

            Log.Information("Начинается регистрация политик CORS.");

            services.AddCors(o => o.AddPolicy("AllowAll", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));

            Log.Information("Регистрация политик CORS успешно завершена.");

            Log.Information("Начинается регистрация сервисов.");

            Log.Information("Начинается регистрация Swagger генератора.");

            services.AddSwaggerDocumentation(this.configuration);

            Log.Information("Регистрация Swagger генератора завершена.");

            // Все сервисы имеют stateless реализацию и могут быть зарегистрированы как singleton.

            Log.Information("Регистрация сервисов успешно завершена.");
            Log.Information("Начинается регистрация фильтра авторизации.");

            // Регистрация фильтра авторизации.
            services.AddScoped<AuthorizationFilter>();
            Log.Information("Регистрация фильтра авторизации успешно завершена.");

            Log.Information("Начинается регистрация сервисов MVC.");
            services.AddMvc();
            Log.Information("Регистрация сервисов MVC успешно завершена.");
            Log.Information("Регистрация сервиса Summary");
            services.AddScoped<DbContext, SummaryContext>();
            services.AddDbContext<SummaryContext>(option =>
                option.UseNpgsql(this.configuration.GetConnectionString("Summary")));
        }
    }
}